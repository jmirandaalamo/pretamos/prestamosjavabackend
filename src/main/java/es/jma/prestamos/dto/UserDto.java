package es.jma.prestamos.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UserDto {

    private String id;

    @NotNull(message = "Email is mandatory")
    @Email(message = "Email should be valid")
    private String email;

    @NotNull(message = "Password is mandatory")
    @Size(min=6)
    private String password;

    @NotNull(message = "Name is mandatory")
    @Size(min=2)
    private String name;

    @NotNull(message = "Surname is mandatory")
    @Size(min=2)
    private String surname;
}

package es.jma.prestamos.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class ErrorDto {

    private HttpStatus status;

    private String message;
}

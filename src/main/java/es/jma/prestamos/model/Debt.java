package es.jma.prestamos.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Getter
@Setter
@Document
public class Debt {

    @Id
    private String id;

    private String name;

    private Date createdAt;

    private Date updatedAt;

    private double quantity;

    private boolean paid;

    private String friendId;

    private String relatedDebtId;

}

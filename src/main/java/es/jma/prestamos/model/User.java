package es.jma.prestamos.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@Document
public class User {

    @Id
    private String id;

    private Date registrationDate;

    @Indexed
    private String email;

    private String password;

    private String name;

    private String surname;

    private String avatar;

    private boolean enabled;

    private List<Friend> friendList;

    private List<Debt> debtList;
}

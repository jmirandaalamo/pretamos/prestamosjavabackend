package es.jma.prestamos.model;

import es.jma.prestamos.enums.TypeOperation;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Getter
@Setter
@Document
public class Operation {

    @Id
    private String id;

    private Date createdAt;

    private TypeOperation typeOperation;

    private double quantity;

    private String debtId;
}

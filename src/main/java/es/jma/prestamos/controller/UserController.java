package es.jma.prestamos.controller;

import es.jma.prestamos.application.Constants;
import es.jma.prestamos.dao.UserDao;
import es.jma.prestamos.dto.UserDto;
import es.jma.prestamos.model.Debt;
import es.jma.prestamos.model.User;
import es.jma.prestamos.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(Constants.Api.BASE)
public class UserController {

    private final UserDao userDao;

    private final UserService userService;

    @GetMapping("/test")
    private User test() {
        User user = new User();
        user.setId("1");
        user.setEmail("test@test.com");
        user.setName("Paco");
        user.setSurname("Porras");

        User user2 = new User();
        user2.setId("2");
        user2.setEmail("test2@test.com");
        user2.setName("Pepe");
        user2.setSurname("Porras");

        Debt debt = new Debt();
        debt.setId(UUID.randomUUID().toString());
        debt.setName("Deuda 1");
        debt.setFriendId("2");
        user.setDebtList(Collections.singletonList(debt));

        this.userDao.save(user);
        this.userDao.save(user2);

        return user;
    }

    @PostMapping(Constants.Api.PUBLIC + "/user")
    public ResponseEntity<UserDto> create(@RequestBody UserDto usertDto) {
        return ResponseEntity.ok(this.userService.create(usertDto));
    }
}

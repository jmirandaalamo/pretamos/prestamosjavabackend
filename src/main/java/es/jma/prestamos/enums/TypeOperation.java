package es.jma.prestamos.enums;

public enum TypeOperation {
    INCREASE,
    DECREASE
}

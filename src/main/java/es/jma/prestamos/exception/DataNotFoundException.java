package es.jma.prestamos.exception;

public class DataNotFoundException extends RuntimeException {

    public DataNotFoundException(String message, Throwable trowable) {
        super(message, trowable);
    }

    public DataNotFoundException(String message) {
        super(message);
    }
}

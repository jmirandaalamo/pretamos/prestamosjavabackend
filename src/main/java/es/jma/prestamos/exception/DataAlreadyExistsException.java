package es.jma.prestamos.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DataAlreadyExistsException extends RuntimeException{

    public DataAlreadyExistsException(String message, Throwable trowable) {
        super(message, trowable);
    }

    public DataAlreadyExistsException(String message) {
        super(message);
    }
}

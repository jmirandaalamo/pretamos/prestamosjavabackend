package es.jma.prestamos.application;

public final class Constants {

    public static final class Api {
        public static final String BASE = "/api";
        public static final String PRIVATE = "/private";
        public static final String PUBLIC = "/public";
    }

    public static final class Security {
        public static final String HEADER = "Authorization";
        public static final String PREFIX = "Bearer ";
        public static final String SECRET = "secretOfPrestamos";

        public static final String CLAIM_AUTHORITIES = "authorities";
    }
}

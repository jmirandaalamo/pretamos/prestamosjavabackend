package es.jma.prestamos.converter;

import es.jma.prestamos.dto.UserDto;
import es.jma.prestamos.model.User;
import org.springframework.core.convert.converter.Converter;

public class UserToUserDto implements Converter<User, UserDto> {
    @Override
    public UserDto convert(User user) {
        UserDto usertDto = new UserDto();

        String id = user.getId();

        if (id != null) {
            usertDto.setId(id);
        }
        usertDto.setEmail(user.getEmail());
        usertDto.setName(user.getName());
        usertDto.setSurname(user.getSurname());
        return usertDto;
    }
}

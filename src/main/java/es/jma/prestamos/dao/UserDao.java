package es.jma.prestamos.dao;

import es.jma.prestamos.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface UserDao extends MongoRepository<User, String> {

    Optional<User> findByEmail(String email);
}

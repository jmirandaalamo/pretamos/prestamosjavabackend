package es.jma.prestamos.service;

import es.jma.prestamos.dao.UserDao;
import es.jma.prestamos.dto.UserDto;
import es.jma.prestamos.exception.DataAlreadyExistsException;
import es.jma.prestamos.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.Date;
import java.util.UUID;

@Service
@Validated
@Transactional
@RequiredArgsConstructor
public class UserService {

    private final UserDao userDao;

    private final PasswordEncoder passwordEncoder;

    private final ConversionService conversionService;

    public UserDto create(@Valid UserDto userDto) {

        String email = userDto.getEmail();
        if (this.userDao.findByEmail(email).isPresent()) {
            throw new DataAlreadyExistsException("User with email " + email + " already exists");
        }

        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setRegistrationDate(new Date());
        user.setEmail(userDto.getEmail());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setEnabled(true);
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());

        this.userDao.save(user);

        return this.conversionService.convert(user, UserDto.class);

    }
}
